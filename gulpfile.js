var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var ssi         = require('browsersync-ssi');
//var uncss       = require('gulp-uncss');
var sass = require('gulp-sass')(require('sass'));
var sassGlob = require('gulp-sass-glob');
var sassLint = require('gulp-sass-lint');
var sourcemaps = require('gulp-sourcemaps');
var useref      = require('gulp-useref');
var uglify      = require('gulp-uglify');
var gulpIf      = require('gulp-if');
var postcss     = require('gulp-postcss');
var cssnano     = require('cssnano');
var autoprefixer = require('autoprefixer');

//var autoprefixer = require("gulp-autoprefixer");
var replace     = require('gulp-replace');
var reload      = browserSync.reload;
var Promise     = require('promise-polyfill');

var src = {
    scss: 'app/styles/**/*.scss',
    scss_top: 'app/styles/*.scss',
    scss_dir: ['app/styles/**'],
    css:  '{app/css/*.css,app/**/css/*.css}',
    html: ['app/ec2-prototype/*.html'],
    js: ['app/js/*.js',
      'app/core/assets/js/*.js']
};
// Compile sass into CSS
gulp.task('sass-compile', function () {
  return gulp.src(src.scss_top)
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({stream:true}));
});

browserSync.emitter.on("service:running", function () {
    console.log("Ready");
});

// Static Server + watching scss/html files
gulp.task('dist-serve', function() {
  browserSync.init({
      server: {
          baseDir: './dist',
          middleware: ssi({
            baseDir:  __dirname + '/dist',
            ext: '.html',
          }),
          routes : {
            '/node_modules' : './node_modules'
          }
      },
      port: 5000,
      notify: true,
      open: false,
      logLevel: "silent"
  });
});

gulp.task('serve', function(cb) {
  browserSync.init({
    server: {
      baseDir: './app',
      middleware: ssi({
        baseDir:  __dirname + '/app',
        ext: '.html',
      }),
      routes : {
        '/node_modules' : './node_modules'
      }
    },
    port: 3000,
    notify: true,
    open: false
  });

  gulp.watch(src.scss_dir).on('change', gulp.series('sass-compile', function(done) {
    done();
  }));
  gulp.watch(src.html).on('change', reload);
  gulp.watch(src.js).on('change', reload);
  gulp.watch(src.css).on('change', reload);
});

gulp.task('useref', function() {
  var plugins = [
    autoprefixer({cascade: false}),
    cssnano()
  ];

  return gulp.src([
    'app/index.html',
    //,
    // 'app/ec2-prototype/index.html'
    // 'app/itc-visioning/index.html',
    'app/seed/index.html',
    // 'app/{academics,admissions}/**/*.html',
    // 'app/seed-minified/index.html',
    // 'app/seed-logo/index.html',
    //'app/{academics,admissions,core,spotlights}/**/*.html'
    //'app/wordpress/index.html'
  ])
  .pipe(useref())
  // Minifies only if it's a JavaScript file
  //.pipe(gulpIf('*.js', uglify()))
  //.pipe(gulpIf('*.css', autoprefixer({cascade: false})))
  //.pipe(gulpIf('*.css', cssnano()))
  .pipe(gulpIf('*.css', sourcemaps.init()))
  .pipe(gulpIf('*.css', postcss(plugins)))
  .pipe(gulpIf('*.css', sourcemaps.write('.')))
  .pipe(gulp.dest('dist'))
});

gulp.task('copy-files', function () {
  return gulp.src([
    'app/_files/**/*',
    'app/_files/components/**/*',
    'app/_files/images/**/*',
    'app/_files/*.html'
  ], { base: 'app/_files' }).pipe(gulp.dest('dist/_files'))
});

gulp.task('copy-slides', function () {
  return gulp.src([
    'app/slides/**/*'
  ], { base: 'app/slides' }).pipe(gulp.dest('dist/slides'))
});

gulp.task('copy-root', function () {
  return gulp.src([
    'app/index.html'
  ], { base: 'app' }).pipe(gulp.dest('dist'))
});

gulp.task('copy-redesign', function () {
  return gulp.src([
    'app/redesign/js/*',
    'app/redesign/img/*'
  ], { base: 'app/redesign' }).pipe(gulp.dest('dist/redesign'))
});

gulp.task('copy-seed-minified', function () {
  return gulp.src([
    'app/seed-minified/**/*'
  ], { base: 'app/seed-minified' }).pipe(gulp.dest('dist/seed-minified'))
});

gulp.task('copy-seed-logo', function () {
  return gulp.src([
    'app/seed-logo/**/*'
  ], { base: 'app/seed-logo' }).pipe(gulp.dest('dist/seed-logo'))
});

gulp.task('copy-spotlights', function () {
  return gulp.src([
    'app/spotlights/**/*',
  ], { base: 'app/spotlights' }).pipe(gulp.dest('dist/spotlights'))
});

gulp.task('copy-welcome', function () {
  return gulp.src([
    'app/welcome-runners/**/*'
  ], { base: 'app/welcome-runners' }).pipe(gulp.dest('dist/welcome-runners'))
});

//$fa-font-path:        "/_files/components/font-awesome/fonts";
gulp.task('copy-icons', function () {
  return gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
      .pipe(gulp.dest('dist/_files/components/font-awesome/fonts'));
});

gulp.task('default', gulp.series('serve', function(done) {
  done();
}));

gulp.task('copy', gulp.series('copy-welcome', 'copy-seed-minified', 'copy-seed-logo', 'copy-root', 'copy-redesign', 'copy-spotlights', function(done) {
  done();
}));

gulp.task('build', gulp.series('copy', 'sass-compile', 'useref', function(done) {
  done();
}));

gulp.task('lhci', gulp.series('copy-root', 'copy-files', 'sass-compile', 'useref', 'dist-serve', function(done){
  done();
}));
