module.exports = function(grunt) {
  grunt.initConfig({
    cascadeDeploy: { // this identifies the cascadeDeploy section
      'default': { // a callable deployment configuration, you can have more than one
        options: {
          url: 'https://walledev.it.utsa.edu', // required http(s)://cascade.yourdomain.edu
          site: 'UTSA-WWWROOT', // optional, a default setting for site that can be overridden by the files section
        },
        files: [
          {src: ['dist/_files/css/cas/*.css'], site: 'UTSA-WWWROOT', dest: '_files/css/', type: 'file'},
          //{src: [], site: '', dest: '', type: 'file', rewriteLinks: true, maintainAbsoluteLinks: true},
          //src globable name pattern for local files
          //site only needed if overriding options.site
          //dest -- a cascade folder path eg '/_internal'
          //type -- for now always file
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-cascade-deploy');
  grunt.registerTask('default', ['cascadeDeploy']);
};
