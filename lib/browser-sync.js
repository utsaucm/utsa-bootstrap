#!/usr/bin/env node

var browserSync = require('browser-sync').create('utsa-bootstrap-npm');
var ssi         = require('browsersync-ssi');

var src = {
    scss: 'app/styles/{top-level,app,bundle,slides-only,wordpress,inauguration}.scss',
    scss_dir: 'app/styles/**',
    css:  'app/css/*.css',
    seed_css:  'app/seed/css/site.css',
    html: '{app/*.html, app/**/*.html}',
    js: 'app/js/*.js'
};

/**
 * Run Browsersync with server config
 */
browserSync.init({
    server: 'app',
    files: ['app/*.html', 'app/css/*.css'],
    middleware: [
      ssi({
        baseDir:  __dirname + '/app',
        ext: '.html',
      })
    ]
});

browserSync.watch('**/*.html').on('change', bs.reload);
browserSync.watch('**/*.css').on('change', bs.reload);
