#!/usr/bin/env node

var sass = require('node-sass');

var glob = require('glob');

var srcPath = __dirname + 'app/styles';
var destPath = __dirname + 'app/css';
var totalDuration = 0;
console.log(sass.info);


glob('app/styles/{top-level,app,bundle,slides-only,wordpress,inauguration}.scss', {}, function (err, files) {
  files.map(function(f) {
    var result = sass.render({
      file: f,
      outFile: destPath + f
    }, function(error, result) {
      if (error) {
        console.log(error.status);
        console.log(error.column);
        console.log(error.message);
        console.log(error.line);
      } else {
        totalDuration += result.stats.duration;
        console.log(result.stats.entry + ', ' + totalDuration);
      }
    });
  });
});
