# UTSA Bootstrap Build System

## Features
- built with gulp (we require multiple outputs/testing)
  - cssnano
  - js minify
- browsersync (for live prototyping)
  - supports includes to mimick IIS #includes
- web dependencies in npm (because dependency management on top of dependency management sucks, looking at your browserfy)


## Setup
```
  $ npm install
```

### For live prototyping (default task)
```
  $ gulp
  live prototype viewable at: http://localhost:3000
```

### For building assets
``` 
    $ gulp build
```    

## App structure and Setup

- gulp build places minified/built assets into the build folder
- assets are built from the app folder, this is folder that is served to you locally from the gulp serve command (default task)
  - SASS source files are in app/styles for example:
    - branding uses the SASS source files app.scss and branding.scss
    - app.css brings in the necessary bootstrap source files, variables and global header/footer sass files
    - branding.scss coordinates sass files for branding content styles
    - gulp build takes care of SASS compilation, concatenation and minification
    - errors in sass are reported on the console during a gulp serve, this may break local prototyping (brwosersync) if you have a sass error

- the app folder has historic mock-ups of current designs being used such as:
  - app/index.html (www.utsa.edu home page)
  - app/today/index.html (utsa today home page)
  - app/sombrilla/index.html (sombrilla mock-up)
  - seed* (cascade seed mock-ups and minified headers)

  ## Updating dependencies

  Web dependencies are managed directly by npm, so updating a dependency entails updating the npm dependency via npm or package.json.
