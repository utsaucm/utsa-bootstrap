var gulp        = require('gulp');
var browserSync = require('browser-sync');
var ssi         = require('browsersync-ssi');
var uncss       = require('gulp-uncss');
var sass        = require('gulp-sass');
var useref      = require('gulp-useref');
var uglify      = require('gulp-uglify');
var gulpIf      = require('gulp-if');
var cssnano     = require('gulp-cssnano');
var imagemin    = require('gulp-imagemin');
var imageminPngquant = require('imagemin-pngquant');
var image = require('gulp-image');
var reload      = browserSync.reload;

var src = {
    scss: 'app/today/styles/today.scss',
    scss_dir: 'app/today/styles/*',
    css:  'app/today/css',
    html: 'app/today/*.html',
    js: 'app/today/js/*.js'
};

// Static Server + watching scss/html files
gulp.task('dist-serve', function() {
  browserSync({
    server: {
      baseDir: './dist',
      middleware: ssi({
        baseDir:  __dirname + '/dist',
        ext: '.html',
        version: '1.4.0'
      })
    }
  });
});

gulp.task('image', function () {
  gulp.src('img-src/*')
    .pipe(image({
      pngquant: true,
      optipng: true,
      zopflipng: true,
      jpegRecompress: false,
      jpegoptim: false,
      mozjpeg: false,
      gifsicle: false,
      svgo: false
    }))
    .pipe(gulp.dest('img'));
});

gulp.task('imagemin', function() {
    gulp.src('img-src/*')
      .pipe(imagemin([imageminPngquant({
        floyd: 0,
        nofs: true,
        quality: "85-100",
        verbose: true
      })]))
      .pipe(gulp.dest('img'));
});

gulp.task('serve', ['sass'], function() {
    browserSync({
      server: {
        baseDir: './app',
        middleware: ssi({
          baseDir:  __dirname + '/app',
          ext: '.html',
          version: '1.4.0'
        })
      }
    });

    gulp.watch(src.scss_dir, ['sass']);
    //gulp.watch(src.html).on('change', reload);
    gulp.watch(src.html, reload);
    gulp.watch(src.slides, reload);
    gulp.watch(src.js, reload);
});

// Compile sass into CSS
gulp.task('sass', function() {
  return gulp.src(src.scss)
    .pipe(sass())
    .pipe(gulp.dest('app/today/css'))
    .pipe(reload({stream: true}));
});

gulp.task('useref', function(){
  return gulp.src(['app/today/*.html'])
    .pipe(useref(
      {
        //searchPath: 'app'
      }))
    // Minifies only if it's a JavaScript file
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'))
});

gulp.task('copy', function () {
  return gulp.src(['app/today/components/**/*', 'app/today/images/**/*'],
    {
      base: 'app/_files'
    }).pipe(gulp.dest('dist/_files'));
});

gulp.task('default', ['serve']);

gulp.task('build', ['sass', 'copy', 'useref']);
