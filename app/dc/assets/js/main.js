(function($) {
	skel.breakpoints({
		xlarge:	'(max-width: 1000px)',
		large:	'(max-width: 900px)',
		medium:	'(max-width: 876px)',
		small:	'(max-width: 700px)',
		xsmall:	'(max-width: 575px)'
	});

	$(function() {
		var $window = $(window),
		$body = $('body');
		// Disable animations/transitions until the page has loaded.
		$body.addClass('is-loading');
		$window.on('load', function() {
			window.setTimeout(function() {
				$body.removeClass('is-loading');
			}, 100);
		});

		// Hero Banner.
		var $hero = $('#hero');
		if ($hero.length > 0) {
			// IE fix.
			if (skel.vars.IEVersion < 12) {
				$window.on('resize', function() {
				var wh = $window.height() * 0.60,
				bh = $hero.height();

				$hero.css('height', 'auto');
				window.setTimeout(function() {
					if (bh < wh)
						$hero.css('height', wh + 'px');
					}, 0);
				});
				$window.on('load', function() {
					$window.triggerHandler('resize');
				});
			}

			// Video check.
			var video = $hero.data('video');
			if (video) {
				$window.on('load.hero', function(e) {
					// Disable hero load event (so it doesn't fire again).
					var videoTag = $('#background-video');
					$window.off('load.hero');
					// Append video if supported.
					if (skel.vars.mobile
						&&	skel.breakpoint('large').active
						&&	!skel.vars.IEVersion > 9)
							$('#background-video').remove();
						//document.getElementById('background-video').play();
				});
			}
			// More button.
			/*
			$banner.find('.more')
			.addClass('scrolly');
			*/
		}
		// Scrolly.
		/*
		$('.scrolly').scrolly();
		*/

		// Initial scroll.
		$window.on('load', function() {
			//$window.trigger('resize');
		});
	});
})(jQuery);
